__author__ = 'beahacker'
def NBAccuracy(features_train, labels_train, features_test, labels_test):
    """ compute the accuracy of your Naive Bayes classifier """
    ### import the sklearn module for GaussianNB
    from sklearn.naive_bayes import GaussianNB
    from time import time

    ### create classifier
    clf = GaussianNB()

    ### fit the classifier on the training features and labels
    t0 = time();
    clf.fit( features_train, labels_train )
    print( "training time: ", round(time() - t0, 3), "s")

    ### use the trained classifier to predict labels for the test features
    t0 = time()
    pred = clf.predict( features_test )
    print( "testing time: ", round(time() - t0, 3), "s")

    ### calculate and return the accuracy on the test data
    ### this is slightly different than the example,
    ### where we just print the accuracy
    ### you might need to import an sklearn module
    from sklearn.metrics import accuracy_score
    accuracy = accuracy_score( labels_test, pred )
    return accuracy